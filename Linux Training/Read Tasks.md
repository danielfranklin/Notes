# Further Reading
---
## Kernel Programming
- tasklets, workerqueue, softirq, ISR
- Virtual Memory, Virtual Addressing
- Address lines / Address spacing / Registers / CPU - ALU
- DMA Controller, DMA Transfer, DMA Channel

## Hardware
- UART
- BUS
- PCIE - SDIO
- GPIO
- USB

## Operating System
- Process Execution
- Program Counter, Stack Pointer, Base Pointer
- Reading Object Dump
- Reading and analysing crash
